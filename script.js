let firstNumber = "";
let secondNumber = "";
let operator = "";
let validNumber = true;
let validOperator = true;

function calculator(first, second, op) {
  if (op === "+") return first + second;
  if (op === "-") return first - second;
  if (op === "*") return first * second;
  if (op === "/") return first / second;
}

while (validNumber) {
  firstNumber = prompt(`First number: ${firstNumber}`);
  secondNumber = prompt(`Second number: ${secondNumber}`);

  if (Number.isNaN(+firstNumber) || Number.isNaN(+secondNumber)) {
    alert("Not a number, please write numbers");
  } else {
    validNumber = false;
  }
}

while (validOperator) {
  operator = prompt(`Operator - + / * : ${operator}`);
  if (
    operator === "+" ||
    operator === "-" ||
    operator === "*" ||
    operator === "/"
  ) {
    validOperator = false;
  } else {
    alert("Not a valid operator");
  }
}

console.log(calculator(+firstNumber, +secondNumber, operator));
